# Requires: pyandoc http://pypi.python.org/pypi/pyandoc/
# (Change path to pandoc binary in core.py before installing package)
# TODO: Preserve span formatting from original webpage

import subprocess
import os
import glob
import shutil
import time
import multiprocessing

SEPARATOR = "\\"

def timing(f):
    def wrap(*args):
        time1 = time.time()
        ret = f(*args)
        time2 = time.time()
        print('World forged in {:.2f} seconds 🎉'.format((time2-time1)*1.0))
        
        return ret
    return wrap

def Refine(globPath, extension, output_path):
    print("Refining stock ⚙️  Converting Markdown to Word")
    pool = multiprocessing.Pool()
    for path in glob.glob(globPath, recursive=True):
        pool.apply_async(process, [path, extension, output_path])
    
    pool.close()
    pool.join()

def process(path, extension, output_path):
    oldPath, filename_ext = os.path.split(path)
    filename = os.path.splitext(filename_ext)[0]

    newPath = oldPath.replace("src", output_path, 1).strip("\\")
    if newPath:
        if not os.path.exists(newPath):
            os.makedirs(newPath)
        newPath = newPath + SEPARATOR
    
    #print(newPath)
    output = newPath + filename + extension
    #print(output)

    #continue
    args = [
        '-s',
        path,
        '-f markdown',
        '-o ' + output
        ]

    subprocess.call("pandoc " + ' '.join(args), shell=True)

def Smelt(globPath, output_filename):
    print("Smelting 🌋  Combining all documents")
    path = glob.glob(globPath, recursive=True)
    input = ' '.join(path)
        
    args = [
        '-s',
        input,
        '-f docx',
        '-o ' + output_filename
        ]

    subprocess.call("pandoc " + ' '.join(args), shell=True)

def ClearMolds(path):
    print("Clearing molds 💥  Deleting /tmp")
    shutil.rmtree(path, ignore_errors=True)

tmp_glob = "src/**/*.md"
tmp_path= "tmp"
tmp_extension = ".docx"
output_glob = "tmp/**/*.docx"
output_filename = "dist\\melor.docx"

@timing
def Forge():
    # World Forge
    print("Heating up the World Forge 🔥")
    
    ClearMolds("./tmp/")
    
    Refine(tmp_glob, tmp_extension, tmp_path)

    Smelt(output_glob, output_filename)
    print("Forging 🔨  Finishing up")
    
    ClearMolds("./tmp/")
    
if __name__ == '__main__':
    Forge()
    number_of_documents = len(glob.glob(tmp_glob, recursive = True))
    print(f"Combined {number_of_documents} documents to one Book 📘")

    # TODO: INSTALL LATEST PANDOC!!!!

