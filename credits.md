---
title: Melor
---

# Bidragande

## Författat av

- David Malmström
- David Israelsson

## Tack till

- Karin Svingen
- Anton Avaki
- Markus Welander
- Victoria Andersson
- Johan Hedkvist
- Johan Ek
- Tobias Öfverberg
- Mikael Widén
- Ibrahim Mehdi
- Tim Fryk
- Eric Ficinius
- Matt Ficinius
- Farhan Leili
- Karl-Fredrik Holmsten
- Patrik Sahlin
- Tua Wester
- Jonathan Iversen-Ejve
- Jonathan Hemlin
- Christian Schrewelius Viklund