# Project Melor

## Build

- Run ```build.py```

## Dependencies

- Python 3.7.0+
- Pandoc X.X.X.X

## MacOs & *nix

All rights reserved &copy; David Malmström 2018
