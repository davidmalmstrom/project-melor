---
title: Döden
---

# Döden

## Vad händer efter döden?

Själen förs till Magistormen och där splittras den och sätts ihop till en ny själ. Där väntar den på att återigen komma in i någon utav världarna. Det är under denna väntetid som en person kan bli återupplivad. Hinner ett enda själsfragment lämna Magistormen är det för sent.

Dör många under en tid kommer många själar finnas i Magistormen och tiden för återupplivning ökar då varje själ har ett kösystem där de som varit där längst får först komma ut i världen.

Först in, först ut.

## Stormvandrare

Du är återfödd med samma själ, kanske även i samma kropp. Du är jagad av alla som är något intresserad av magi. Du har kvar svaga minnen av dina förra liv. Ibland kan du få visioner från förr som du upplever som verklighet. De få i världen som är Stormvandrare har dock levt stora liv, Arylen Rosklo, dotter till en kung under första tidsåldern och förlovad till Trollkarlskungen i den nionde tidsåldern. Castiel du Sin, piratkung under den tolfte tidsåldern och stor äventyrare och senare ledare över orcherna i Melor i den trettonde tidsåldern, under den fjortonde tidsåldern återgick han till att bli piratkung.

## Odöda

Zombies och skelett är bara kroppar animerade med magi.

Spöken och gastar har behållit sin själ i världen. Antingen via viljestyrka, de har valt att stanna för att hämnats, hålls kvar av en likgasturna eller fångad av en magiker.