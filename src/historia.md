# Historia

## Första tidsåldern

### Universum skapas genom en magistorm

A long time ago there was nothing, then the Storm of Magic came into being. Nobody knows why, not even the Entities, all anyone knows is that the storm carries immense magical power.

The Storm of Magic is giant spiraling storm that encompasses all the worlds. The magical energy in the storm is what fuels a mage's power. The storm also holds all the souls of all worlds who have died.

### The Entities start to emerge

All pronouns regarding the entities come from how beings of the worlds have seen their vessels, themselves have no concept of gender or sex.

After a quite long time of nothing happening in the universe parts of magic energy started forming orbs of energy, with time they started to gather more and more of this arcane power. The first race of the universe formed as beings of pure magic power, they were born from the will of the storm.

First emerged the entity Time, he sensed that he was in complete control of time itself. He spent what the elves later would call an eternity, alone. The time had an impact on him as his willpower started to break down he started to feel the cold emptiness of an endless storm of magical power with nothing in it. He felt sad and he noticed that energy started to form into an orb, the process was however slow and his sorrow immediate. In an instant the process had been speedup by him and another entity emerged, Feeling. Time was happy that he had another person to share the universe with. Feeling was also glad to see that he was not alone, he greeted Time and they became good friends, seeing as there was only two people in the universe.

Time and Feeling fell in love with each other and from their love came another entity into the universe, Life. As a reaction to this the storm started to form another entity that would emerge much later, Death.

Feeling and Life started to test what their powers could do. Their experiments with power of the storm led to a new race of people, it was a frail being and they named it Agrun. It did not live long in the turbulent magical place that the entities call home. The storm took the life of this new race and ate its life-energy until there was nothing left. Life was distraught and became sad, this sadness only grew as more more tests of their abilities garnered no lasting results. This sadness increased her will to create something lasting, she drew upon the power of the arcane storm until she could no longer knew who she was.

#### Agra is created

Life sparkled with power not knowing how to contain it, how to control it. Time tried to help his daughter guide this power. What did end up happening is the first world was created, Life named her new world Agra after the first beings the Agrun that she and her mother had created so long ago. Looking at what they had created, a place in the storm where others could be safe. She and her father Time recreated those frail beings from before, the Agrun. The entities saw to it that the Agrun would survive in this world and watched as new race took its first stumbling steps.

Death had not emerged yet so the world of Agra or its inhabitants will never die.Melor skapas

- När Tid och Livet skapat Arkadia var de så pass svaga att de inte kunde stoppa Rum från att skapa en egen värld.

- När Tid fick reda på detta gick hon till Livet och bad om hjälp att hindra Rum från att breda ut sin värld för stort.

- Livet ville dock inte det då Rum lovat sin värld Melor till henne.

- Tid blev då sur på sin tvilling och förbannade Melor till att vara bunden till sig själv. Detta skapade den första tidsåldern.

#### Melor skapas

Some time later Space would emerge

#### Cala skapas

Känsla kände sig svartsjuk på Rum, Tid och Livet som skapat egna världar. Därför skapade Cala, en egen värld där alla känslor skulle få vara helt fria.

### Fysiska lagar skapas av väsen med hjälp av kontrakt

#### Ljus ägnar sin tid till Melor

Ljus reste till Melor och blev förälskad i den. Därför beslutade Ljus sig för att stanna där och slöt ett kontrakt med Livet som fått Melor av Rum där det föralltid skall vara ljust på Melor.

#### Människorna skapas

Livet och Döden skapade ett kontrakt för att göra sig en egen varelse. De kommer överens om att varelsen skall dö vid 100 års ålder och att de skall vara mångsidiga, därför ges de inga fördelar, men ej heller några nackdelar.

#### Livet blir förolämpad och skapar drakar som en temper-tantrum

#### En mäktig fe slöt ett kontrakt med Livet.

Feen Mirala födde tre barn som togs från henne vid livets träd av Suraka. Suraka slöt ett kontrakt med Livet där feerna skulle få leva för evigt om de tre barnen fördes till en annan värld. Där skulle dessa barn skapa en ny ras, ett samhälle och dö i feernas ställe.

#### Ljus var tvungen att ge hälften av sin tid till Mörker

Ljus och Mörker träffades på Omen för att samtala om det som hänt under de senaste tusentals åren sedan de sist träffades. De stannade länge i Melor och vandrade runt bland dess skogar, öknar, hav och riken. Överallt såg de hur de olika varelserna betedde sig och hur deras samhällen såg ut. Det var då som Mörker startade ett vad med Ljus om vilken varelse som var den bästa. Inte den absolut bästa i världen utan bara bäst på vad de just nu höll på med. De kom motvilligt överens om att om Ljus vann skulle Mörker ge upp sin tid i Cala. Om Mörker vann skulle Ljus ge upp halva sin tid i Melor. Med överenskommelsen klar skred de till verket. De tävlade om vilken människa som kunde lyfta upp vatten snabbast. Till slut stod det mellan Ljus förkämpe en högalv och Mörkers förkämpe en människa. De slogs tillsammans mot en drake. Det var människan som gav draken dödsstöten, och med det gav Mörker vinsten för hela tävlingen. Ljus som förlorat tyckte att människan hade fuskat genom att döda draken när alven hade redan stött sitt spjut i draken och mer eller mindre bara gick fram och gav den en nådastöt. Mörker som är rädd för Ljus gav med sig och lät Ljus regera litegrann under sin tid. Ljus skapade då månljuset och stjärnorna för att varelser på Melor skall kunna få hopp under den mörka perioden av dygnet.

### Sociala strukturer skapas av alla intelligenta raser

#### Ett krig mellan alver och människor.

### Arylen Rosklo föds för första gången

- En mystisk människa kallad Arylen Rosklo föds.

- Hennes födsel utlystes av ett stort vitt sken från himlen.

- Hon föddes till den mäktiga krigarkungen Tahar Yargai från riket Adrar.

## Andra tidsåldern

## Tredje tidsåldern

## Fjärde tidsåldern

## Femte tidsåldern

## Sjätte tidsåldern

### Ockultisten uppstår

### Dvärgarna skapas

- De var ett experiment av Svartalverna på människor för att skapa de ultimata gruvarbetarna.

- De behövde fler slavar till sina gruvor

- De tog människoslavar och mutilerade dem till att passa för gruvbruk

- Gjorde dem fertila och snabbade på uppväxten

- Bra för att avla fram fler av dem

- Bra för att få fram arbetare fortare

- Gavs även ett begär för metall så att de suktade efter att gå ned i gruvorna

- Se bättre i mörker

- Slapp köpa ljus

- Tåligare hud

- Slapp köpa skor

- Tog bort deras hårväxt så att de inte skulle sprida sjukdomar

- Därför anses ett präktigt skägg vara ett hån mot svartalverna och främjas hos dvärgarna

- Gav dem längre liv

#### Rebelluppror

Flertal under åren.

#### Exodus

- 35 % av alla slavar flyr

- Skapar nytt samhälle

- vill bli människoliknande igen/inte slavliknande

- går fel

- de får svagare versionen av sina egenskaper

- Ser bara lite bättre i mörker AKA nightvision

## Sjunde tidsåldern

### Alla imperiers fall

#### I desperation gömmer människorna sitt arv

#### All den kända kunskapen vävdes samman i sånger, för att aldrig glömma den fruktansvärda tiden. Den här låten fortsätter att skrivas/sjungas.

#### Orcherna uppfinner sitt egna språk Olur

## Åttonde tidsåldern

### Kontraktet avseende isens smältning slutar och den 8:e ålder skapas.

### Dvärgkungen smiddes in i de djupa bergen för att förnya kontraktet, i sju cykler till.

### Halvlängdmännen hittas i de djupa bergen

## Nionde tidsåldern

### Trollkarlskungens skapar riket Derlis

Skapas i början av den nionde tidsåldern av Trollkarlskungen. Trollkarlskungen skapade och styrde över riket Derlis.

### Den insmidda dvärgkungen hittas och ett tempel byggs

#### Plundringen av dvärgkungens tempel av Orcherna

### Arylen Rosklo föds för den tredje gången

#### Mordet på Arylen Rosklo under hennes bröllop med Trollkarlskungen

**Vad hände med Arylen Rosklo och Trollkarlskungen under deras bröllop?** Trollkarlskungens syster skar halsen av Arylen Rosklo, trollade så att de bytte utseende och brudtärnan fick ta skulden för döden på Trollkarlskungens "syster".

## Tionde tidsåldern

### Gastkungens zombiepest

#### Arylen Rosklo återföds

#### Arylen Rosklo mördar Trollkarlskungen för hans svek på deras bröllop

Arylen förhäxar även honom att alltid vara ett mellanting av liv och död. Trollkarlskungen blir Gastkungen.

#### Arylen förlorar stödet från människorna till förmån för Gastkungen

Arylen dödade Gastkungens syster och försöker ta makten över landet i egenskap av att vara drottning.

Folket av Derlis respekterade fortfarande sin förre kung och ställde sig inte bakom Arylens försök att ta tronen.

#### Gastkungen försöker upphäva kontraktet om att saker dör.

### Nedgången av den sista stora alvstaden Sen Alora

Den sista stora alvstaden Sen Alora faller under de krig som blir mellan Gastkungen och Arylen.

## Elfte tidsåldern

## Tolfte tidsåldern

### Det stora kriget av gastkungen mot alla andra

Gastkungen förlorade stödet hos folket av Derlis som mer och mer under den elfte tidsåldern hade börjat följa drakkejsaren.

Ett krig för att besluta vem som verkligen styrde började och de övriga faktionerna ställde sig på drakkejsarens sida.

Alvdrottningen gjorde detta då alverna fortfarande var bittra över Sene Alora.

### Det stora glasyrkriget

För att fira vinsten över gastkungen ställde drakkejsaren till med en väldig fest. Festen krävde bakverk och drakkejsaren utlovade att den som bakade det bästa verket skulle få en hedersplats i hovet till denne och dennes ättlingar så länge drakkejsaren styrde.

### Halvlängdsmännens filosofiska konflikt om en nomadisk livsstil eller bosatta liv, skapar skuggprinsen.

### Piratkungens fall

Orchen Castiel du Sin som är piratkungen faller i kamp under ett myteri.

## Trettonde tidsåldern

Trettonde tidsåldern tog vid när Piratkungen dog i den Tolfte tidsåldern. Det aktuella datumet är 13.6498.

### 13.2475 Piratkungen återföds

Castiel du Sin återföds som en halvlängdsman.

### 13.2495 - Fjärde Järnhavskriget

- Järnhavet

- Går dåligt för Sverkers klan

- Hertig Stauflos

- 4 klaner

### Rosa kristallernas kamp

Kampen om de rosa kristallerna påbörjades 13.2498 när en stor rosa kristall störtade från himmelen och gick i sju delar. Dessa mindre kristaller spreds runt i kring Drakhavet.

#### Kristallen i Arkklostret

- Castiel du Sin finner en kristall utanför Harmoni, Spirornas stad.

- - I närheten av Veteklippa i ruinerna efter Arkklostret.

- Tillsammans med sina färdkamrater.

- De lämnar iväg kristallen till drakimperiet.

#### Kristallen utanför Östpark

- Castiel du Sin finner en kristall i Nyhamn.

#### Kristallen i Eldgrottorna

- Castiel du Sin finner en kristall i Eld.

### Ockultisten uppstår igen

Ockultisten som inte setts till sedan den sjätte tidsåldern återuppstår. Nu i kroppen på kunskapstörstaren Hans von Ruben.

### Dödens kontrakt förnyas

#### Castiel du Sin förnyar Dödens kontrakt.

##### Del 1

Denna del av kontraktet är brutet då Lenore har dött. Har lovat döden att se till att Lenore uppfyller sitt öde genom att döda Gastkungen och bli dödens drottning. För detta ska alla farare, resenärer, pirater bli belönade med att inte hämtas så tidigt samt överleva sjukdomar lättare. Castiel du Sin skall även inte hämtas vid sin död. Fakta: Alla andra lever kortare och blir mer mottagliga mot sjukdomar, alla andra varelser level 25 år kortare. Tid kommer dock att påverka Castiel du Sin. För en människa så betyder de att de lever till 75 år istället för 100 år.

##### Del 2

Har lovat döden att hjälpa Erus följeslagare i deras svårigheter. För detta skall Castiel du Sin kunna hålla kvar en person i denna värld när hin dör. Vill han hålla en till försvinner den första. Fakta: Personen som hålls kvar blir levande död, odöd.

##### Del 3

Alver är inte längre odödliga. För detta skall Castiel du Sin överleva kristallens effekt. Fakta: Castiel du Sin är dödens kärl.

## Fjortonde tidsåldern

Den fjortonde tidsåldern startade nära slutet av De rosa kristallernas kamp där Dvärgkungen och Orchfursten dog, Ärkemagikern, Skuggprinsen och Drakkejsaren drevs på flykt, Översteprästinnan lämnade Drakkejsardömet.

- Drakkejsardömet splittrades i fyra riken.

- Alverna säkrade upp sina skogar i norr.

- Dvärgarna i bergen norr om Drakkejsardömet dödades av Orchfurstens arméer ledda av Piratkungen Castiel du Sin.

- Skuggprinsens son tog över titeln.

- En vek magiker, Jasper Vargklot fick tag på Världens Eviga Evighet: Den kompletta samlingen i 16 volymer och tillsammans med sin röda drake blev en stor maktspelare.