---
title: Magi
---

# Magi

## Trollkarlar

Trollkarlar som använder sin intelligens för att låta magi flöda till dem och forma den till trollformler. De studerar och lär sig sättet att utföra magi på.

Gillar det du skrivit. Skulle vrida wizards mer till studie av världens lagar och kontrakt. Intelligens för att det kräver att memorisera och hålla mycket formler och matematiska uträkningar i huvudet osv.

Mäktiga magiker kanske söker att forma egna formler genom att skapa nya kontrakt eller kryphåll i dem.

## Häxmästare

Häxmästare använder sin charm för att lura magin från magistormen att flöda till dem i sitt färdiga skede och använda magin.

## Druider

Druider får sin kraft från magin som genomsyrar naturen.

## Ordenkrigare

Ordenkrigare får sin magi genom att avge löften/kontrakt?

## Skald

Skalder har lite magin som de får genom att alstra energin i deras uppträdande, sång, musik, dans...

## Präster

Enligt bokens regler väljer de några domäner som helande, rättvisa, krig, etc. som är en form av specialisering, kanske är relevant.

Deras medvetande och tro blir en kraft i magi stormen som ger den en resonans. Den påverkar stormen. Blir en del av den så att säga. Så tron i sig ger inte styrka. Men tron av dom som gått vidare och dött. Därav är det viktigt vilken tro dom som dör har. För det påverkar religionerna som levers styrka. Därför kan också uråldriga religioner nu utdöda fortfarande hålla styrka. Ser magi stormen som en levande organism som påverkas av dom som uppstiger i den. Att deras tro upplevelse och filosofi dom hade som levande blir del av den. Därför är martyrer en stor grej i religioner. Ang helning har jag ingen bra fluff förklaring att föreslå ännu. Kanske så att magi påverkar och manipulerar magi stormen medan dem som är clerics (hur man nu blir cleric vet vi fortfarande inte). kan dra kraft direkt ifrån den, livsenergi? Och infusa den i folk därav hela dem med /själfragment/ Mmm typ, de som skapar resonans kan dra sin tros själars energi? Så dom kanske inte vill hela ovärdiga. För att dom slösar sin tros kraft. Nekromanti förklaras även då. Älskar tanken på en as hög levlad cleric bad-guy av en utdöd religion. Som har batterier med kraft från att hans religion var stark men som för tillfället har "begränsat bränsle" att dra på. Så att han är typ as mäktig men vill inte unleasha sin kraft då den inte är unlimited. Tänker att clerics kanske blir infusade med fragment av själstormen.

Döda personer av tron som dessa clerics representerar, kanske används i dessa initiations riter. Olika religioner gör det på olika sätt.

Animistiska religioner kanske låter deras präster förtära blodet av dem döda, eller röka deras torkade fett/kött. Mer moderna monotoistiska religioner kanske har mer symboliska initiationsriter. Tex som kristendomen där man "äter jesus blod och kropp" men dricker urvattnat kasst vin och några halvtaskiga shaskiga kex. Andra kanske brännmärker dig. Ser en fanatisk eld-sekt där dom brännmärker sina clerics och genom den handlingen binder personens "spirit/ande" till den del av själstormen associerad med denna eld-dyrkan. Så tron i sig, är inte det som påverkar styrkan av religionerna utan. Tron och styrkan tron av dom som avlider? Därav vill tex krigs gudarna (se oden eller storme) att sina krigare inte blir vilse i stormen. Kanske också varför så många religioner är PISSED OFF över undeads? Eftersom du då berövar deras tro styrka. I och med att när dom passerar till efterlivet blir dom del av magistormen och deras liv, erfarenheter och tro osv färgar ju av sig på magi stormens flöde. Gör på så sätt den religonen starkare. Att göra en av dessa personer, speciellt om den var extremt fanatisk och dedikerad i sitt liv.. Till en odöd är på så sätt ganska big deal att hata. Nya kulter är ganska värdelösa rill en början Men vad händer med nya kulter? Dom blir förföljda = martyrer = snart bevisas religionens sanning Så nya religioner som har många som blir hårt prövade i sin tro förföljda och dödade för den.. Har potential att få starka clerics (eller fluff wise svaga clerica som snabbt blir starka eftersom så då präster får så mycket kraft) Vilket är varför vissa nya religioner sprids som löpeld.

Exempel hur en religion kan uppstå.

Ett folk är slavar

Det börjas bildas en uppfattning om karma, om rättvisa

Att vi kommer uthärda och bli belönade och fria. Efter att denna uppfattning spridits och många dödats så kanske en person tillslut ger teven på divine powers

Kanske något symboliskt händer som sedan blir endel av relgionens riter

Tex en person tappar korgen med grödor hen plockat och vägrar ta upp den. Blir piskad men får sina sår helade eller känner ingen smärta

Sen kanske att skära loss rep Rut händer blir en symbolisk akt för denna kyrka
