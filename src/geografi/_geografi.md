# Geografi

## Melor

Består av två kontinenter och ett flertal öar.

Den västra kontinenten heter Tavros som betyder *den vida slätten* på kerania, den namngavs av dem som var ett av det första folkslaget på Melor och var primärt ett stäppfolk från början. Alverna kallar kontinenten för téne-rakas, *den andra skogen*.

Den östra kontinenten heter nubi-rakas och betyder den stora skogen på alviska. De första alverna hade aldrig sett en sådan stor skog som den som de trädde in i när de kom till Melor. På kerania kallas den för Nurakas.

## Cala

Demoner och djävlar

## Agra

Féer

## Öar

## Vindlösa öarna

### Halvlings textilland

Land som styrs av halvlingshandelsmän och textilskapare. De bor på en stor ö, odlar egna textilier, bomull, linne.

Byggt ovanpå en dvärgutpost

Dvärgarna tog över en mörkeralvsruin under marken på ön

De skapar slitstarka tyger då de använder hemliga recept och ingredienser från bland annat spindlars silke.

Tyg för äventyrare, slitstarka saker, även silke och siden.