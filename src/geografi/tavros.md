# Tavros

## Utdöda

### Adrar

#### Tejita

Huvudstad i Adrar

### Estaria

Spred språket istina, de kallas nu för gammelistina.

### Drakkejsardömet

Talar kerania som är en vidarutveckling av istina.

#### Derlis

Namnet på Drakkejsardömet när trollkarlskungen fortfarande styrde över det.

## Itaus

## Västmuria

### Aksis

#### Tavernan Svarta Hummern

För att få tillgång till Gyllene tigrarna måste man beställa Mandarinsorbet och lägga upp tre fingrar på bardisken. En vakt som är en suput kan titta åt andra hållet för en summa pengar. Han vaktar bara våning -1. Han går att träffa på tavernan Svarta hummern. Han vill ha en årslön, 60 gm för att titta åt andra hållet. Stor i käften, speciellt när han dricker.

#### Tjuvsällskapet Gyllene tigrarna

Ledaren Reinald Le Strat vill följa med på heisten då han vill sno något i valvet. Gör han det kommer han att kunna hota hans fars position som Skuggprinsen.

#### Mentalsjukhuset Solskogen

För att få tillgång måste man vara släkt med patienten.

#### Cressontornet

Luella Cresson är en magiker likt sina förfäder och bor i ett torn i Axis. Hennes ättling Travon Cresson byggde valvet på beställning för i den elfte tidsåldern. Luella är en gammal kvinna har hans gamla anteckningsböcker kvar men informationen är tusen år gammal.

### Bittraskogen

### Horisont

#### Nyhorisont???

#### Askonia, Institut för magiska studier

## Skugghamnen

## Sydmuria

## Östmuria

### Nyhamn

### Östpark

Borgmästare Hamza

## Vilda skogen

### Magikern Otyna

### Grottorna vid Eld

## De fria nordstaterna

### Skogen Kindon

Skog och klippor (Norrlandsjungelskogar)

#### Eliandras torn

Mistress of Ice

#### Mäster Henreys stuga

#### Spindelrike

### Veteklippa

Från högländerna i norr skjuter Veteklippa ut från de trädbeklädda kullarna. Där odlar man ett speciellt vete som sägs att när man bakar bröd på det så kan man bli botad både från sjukdomar och onda andar.

Där finns en liten ölstuga med tillhörande sjukstuga. Från Gilbert kan man få ett gott veteöl och syster Rose kan ta hand om dina sår.

### Klosterruinen Ark

En dags vandring norr från Veteklippa ligger en ruin från ett gammalt kloster. Hugget i stenen ovanför den fortfarande stående ingången står namnet ARK.

## Concord

En stadsstat styrd av alver, dvärgar och halvlingblod.

### Historia

Under de rosa kristallernas kamp var Armbrek Glödblod  en inflytelserik dvärgisk handelsman gift med shalynn sorevala, en svartalvisk adelsdam från esamila. Han blev mördad under mystiska omständigheter och hans frus släkt sökte hämnd genom att hyra lönnmördare. Castiel du Sin var en av de inblandade och med sina kamrater var de lönnmördare inget hot. Sedan dess har pirater inte längre någon fristad i Concord och tillåts endast hålla till i hamnen med risk att fängslas om de rör sig längre in i staden.

### Viktiga platser

#### Enögda Poppy

En bar i hamnen där många pirater, hälare och andra skumma personer dricker bort mynt, sorger och ånger.

### Viktiga personer

#### Elissa Jennings

Dotter till piraten Serenity ”Red” Jennings.

## Felicity

Det finns bebyggelse på öns kuster men öns inland består av tät skog närmast djungel där stora kattdjur har sina revir.

### Prinshamn

Prinshamn ligger på den östra delan av Felicity och är en stor hamnstad. Det är få skepp som har Prinshamn som destination utan här säljer en olagliga varor som inte platsar i Innanhavets hamnar. Det är också vanligt att man lastar om skepp här till mindre skutor eller får reparationer i det enorma varvet.

Den livliga gojan är ett populärt och mysigt värdshus på de övre delarna av staden.

Castiel du Sin har byggt upp det gamla fortet som fanns på klippan som ser ut över hamnen. Här håller han hov när han inte är ute och seglar. Det ryktas om att det ska finnas en krypta under fortet där Piratkungen håller demoner tillfånga.

## esamila

Mörkaralvsrike

### Viktiga personer och fraktioner

#### torahtáh

Sýradar Toratáh

#### sorevala

sorevala är den näst mäktigaste adelsfamiljen i malenor efter toratáh. Under de rosa kristallernas kamp mördades en av de yngre kvinnorna, shalynn som var gift med dvärgen Armbrek Glödblod i Concord. sorevala-ätten blev rasande och hyrde tre gånger lönnmördare för att få hämnd, men alla tre misslyckades. Sedan dess hyser de ett stort förakt mot pirater och alla som tillhör Piratkungen. De har lyckats skapa en del inom esamilas flotta som är specifikt piratjägare. Dessa piratjägare heter *relaseku sus-roti o vikase* löst översatt till Vattenslavarnas död.

### poraka, Mörka månens skog

Här bor de flesta mörkeralver i Melor.

### malenor

Staden som Aluna kommer från.

### syhasahara

En utpost i söder närmast Drakhall.

### vika–ku pu, Dödens kyss

En gammal grotta där bland annat Rusorth den vite bodde innan han blev iväg jagad av Aluna. Rusorth hade Världens Eviga Evighet: Den kompletta samlingen i 16 volymer bland sina tillhörigheter och den togs av Aluna.

## Drakhall

En stadsstat.

### Kiströtas grav

TODO: Kopiera texten från OneNote