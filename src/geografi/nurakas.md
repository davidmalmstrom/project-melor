# Nurakas

## Kerajaan

- Franska

- Arabiska

- Marocko

- Tunisien

- Indien

- Pakistan

Ahl är en definitiv markör framför ord.

### Ekonomi

Kryddor är den största exportvaran från Kerajaan och de vanligaste är peppar, kanel, kryddnejlika, muskot, ingefära, saffran, kardemumma, koriander, spiskummin, gurkmeja, muskotblomma, anis, kummin och senap.

### Klimat

Påminner om Indien och Pakistan.

- Djungler

- Öknar

- Berg

### Kultur

Influenser från Nordafrika och nordvästra Indien.

### Hälsningsfraser

En vanlig hälsningsfras "Fred var med dig" ofta ackompanjerat med en namaste-rörelse. En mer formell och religiös hälsning är "Jag bugar inför själen inom dig". Den mest formella och underkastande hälsningen är att knäböja, nudda huvud i marken och visa sina handflator uppåt. Det tyder på full underkastelse och blottar ens hals samt visar att man inte bär något vapen i händerna.

### Familj

Familjen är väldigt viktig i Kerajaan och sitt familjenamn är vördat. Familjen består ofta av en expanderad familj med kusiner, mor- och farföräldrar.

### Mat

På ställen där det är lite torrare odlas vete och där används ofta couscous som en bas i matlagningen. På blötare ställen odlas det ofta ris istället. Av vetet görs det även bröd. Man kan även räkna in vete som en krydda som Kerajaan exporterar till andra delar av Melor.

Myrqa är en vanlig rätt som tar sitt namn från lerkärlet som maten lagas i. Det är rätter som tar lång tid att laga och kan liknas vid en gryta. Oftast görs det med kött, fågel eller fisk, tillsammans med grönsaker. Många olika kryddor används och det finns allt från de starka grönsaksmyrqa, en myrqa gjord på lamm är ofta något sötare och har torkad frukt i sig och de finaste myrqa innehåller nötkött och dyra kryddor som saffran. Ett kerajaanskt ordspåk är att: "Du kan känna doften av din mors myrqa på en dags avstånd". Alla äter myrqa i Kerajaan och kan ses som deras nationalrätt. Lerkärlets lock är format så att ångan kondenserar och rinner ned i grytan vilket då kräver minimalt med vatten.

Den tradionella Paee är även väldigt vanliga, en slags luftig paj som både är salt och söt. Den fylls ofta med en kryddad kött och grönsaksröra. Paee serveras ofta som en förrätt för större måltider.

### Ahl Sajaroon

Huvudstaden i Kerajaan heter traditionellt Ahl Sajaar Hajaroon Sadaq som betyder Den tusenfaldiga templens stad. I vardagligt tal kallas den dock Ahl Sajaroon eller bara Sajaroon som har samma betydelse men är nyare.

Här styr Qasaala Amir (Kryddprinsen) från sitt palats.

### Agadesh

Kerajaans näst största stad.

### Ahl Sarquet

Liten bergsby.

### Skelettet av draken Sorante

Vid floden mellan Agadesh och Ahl Sajaroon ligger skelettet av draken Sorante äterskan av allt. Hon nedgjordes av hjälten Gwirstenn för drygt 200 år sedan. Man har inte vågat att röra hennes skelett på grund av de magiska symbolerna som var ristade på skelettet. Det var en incident strax efteråt när "två dussin mannar dog när så fort de vidrörde benen."

## Heradi

### Maisir

## Istra

### Historia

**14.12**
Mordecai släpper lös en Stormpåse som skickar iväg en enorm storm som försätter stadsstaten Istra i kaos. Istra faller snabbt till nekromantiker, demoner och ekonomiskt fördärv.

### Styrelseskick

**Kollegiet**
Staden styrs av kollegiet, bestående av 47 personer. De fyra stora handelsfamiljerna har sex varsina personer, de övriga 23 personerna tillhör flertalet mindre familjer. Varje person har en röst vilket gör att de stora handelsfamiljerna har sex röster var i alla frågor, de kan på så sätt få en egen majoritet. Varje familj väljer själv sina representanter i detta råd.

### Istra

#### Namn på staden

- **Kerania**: Istra / Döpt efter det gamla ordet för platsen som var Histiri

- **Alviska**: sus-falenas kana / Myntens hem

- **Dvärgiska**: Geldstadna / Myntstaden

- **Orchiska**: Mesto trgovine / Handelsplats**en**

- **Andor**: Geldbluth / Guldblod

- **Tawil**: Almakan falemal qawine / Platsen där pengar styr

#### Rådet

Kollegiet väljer en Intrigmakare, Kansler, Kommendör, Myntmästare och Stadens hand.

Ingen av de 4 stora familjerna brukar vara Stadens Hand. Tidigare fanns det en 5:e stor familj vid namn Omdite varav en av dess medlemmar fick positionen Stadens Hand. Det slutade med att personen blev maktfullkomlig despot. Staden var tvungen att bekämpa den 5:e familjen och brände i slutändan ner familjens residens med hjälp av magiker. De tappade kontroll över elden och en del av staden brändes ner. Sedan dess har magiker varit tvungna att skaffa licens för att praktisera sin konst i staden.

### **Viktiga fraktioner** & Handelsfamiljerna

#### Gnomhuset Gerold Hammarnäve

#### Människohuset Talmera Rosfjäder

#### Människohuset Bavdarr Stensköld

#### Skogsalvshuset Eradona Guldskog

### Förslag på namn:

- Aunsellus
- Mainfroi
- Namos
- Vodar
- Fibris
- Boreus
- Oktia
- Sodona
- Höstsorg
- Sommarstål
- Skugghjälm
- Vintersky
- Ljussköld
- Grönhorn
- Krigsande
- Drömkross
- Askmoln
- Järnristare
- Gryningssång
- Månöga
- Sköldlöv

### Dvärgarna

Geldklanen bor under staden där håller dem kontroll över värme källor i form av lava sjöar. Dessa sjöar används sedan för att producera värme som förs genom rör upp till stadens olika kvarter. För dessa tjänster så erhåller dem god betalning. Det är vanligt att rikare personer har elaborerade värme system genom sina hus. Det kan handla om att de kan få varmt vatten till sitt badrum, golvvärme osv. Medan för de som har lite mindre pengar så handlar det främst om bas-uppvärmning under vinter tid. Många har inte råd med denna typ av tjänster överhuvudtaget. Stadens badhus köper även dem uppvärmning utav dvärgarna. Det finns också bagare som köper värme till sina ugnar. På grund av brandrisken i staden så föredrar de flesta att använda lavaugnar, eller ångugnar. Denna klan med dvärgar äger tillåtelse till vissa sorters hantverk men inte alla då staden har skråväsende. Bland annat har dvärgarna monopol på tillverkningen av mynt, grovsmide samt vissa sorters vapen och rustningar. Det förekommer att dvärgsmeder i staden praktiserar finsmide men inte tar betalt för dessa verk då det skulle bryta mot stadens lagar. Därför är dvärgiskt finsmide en importvara i staden trots att det finns en ganska stor dvärgklan här. Däremot kan en tacksam dvärgsmed ge bort verk till folk som gjort stora saker för dem. Tidigare så befolkades underjorden av andra dvärgar, dessa grävde för djupt och historien talar om en fruktansvärd undergång för dessa. Med tiden så kom nya dvärgar, Geldklanen, som etablerade sig under staden. Det var också då som lava sjöarna hittades. Dvärgarna har alltid haft en konflikt med mörkeralverna som också bor i underjorden. De två folkslagen har gränsdragningar över vem som har rätt till vilken del av underjorden. Ibland innebär detta konflikter över vem som egentligen har rätt till vad, dessa konflikter är extra vanliga när någon tex hittar en ny ådra av malm. Generellt så är det uppdelat enligt väderstrecken där dvärgarna bor i öst och norr medan mörkeralverna bor i väster och söder. Dvärgarnas äldste har mycket inflytande bland dem, det finns dock ingen mer formell hierarki bland dem.

### Mörkeralverna

Mörkeralverna bor under staden likt dvärgarna men de har lite till övers för dem. Det ser sig själva inte som en del av staden men är en del av den. De har en egen monark som styr över dem som just nu är kungen Aravel ”Flodkorp” Reva–koka.

De sysslar främst med smide av vapen och rustningar för export. De har fått tillåtelse av smidesskrået att hålla på med finsmide såsom smycken, detaljer till vapen och verktyg, något som förargar dvärgarna. Från vapenskrået har de fått lova att smida dolkar, pilbågar, armborst och dess pilar & skäktor.

### Skråverksamhet

- Vapen
- Smide
- Textilier
- Mat
- Rusdryck
- Handel
- Legoknekt
- Gladiator
- Trädgård

### Stadsdistrikt

Det finns olika distrikt i staden och dessa sköts av en distrikt mästare som blir utsedd utav stadens hand eller stadens kollegie. Det enda kravet på att ställa upp inför dessa som en utav dem som de får välja är att du skall bo i området.

#### Västra delen av Istra, FATTIG PLATS

### Sklavmark