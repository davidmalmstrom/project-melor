# Vaköarna

Smaragdgrottorna

## Järnhavet

### Skeppsklippan

### Frostön

### Eldön

## Sternland

Det lysande stjärnornas land är Lehkisternas högsäte och heligaste plats, i slutet av den trettonde tidsåldern begav sig Stjärnprästinnan söder ut under kaoset som fanns i Drakkejsardömet. Hon återvände till Lehkismens heligaste plats som sedan många hundra år varit fyllt med demoner och onda bestar. I desperation slöt hon avtal med djävlar och hyrsvärd och tillsammans med sina egna trupper och präster lyckades hon tillslut att återta stora delar av landet. Sternekk flyttade stora delar av sina arméer för att hjälpa henne, även fast han har svårt att hålla stånd mot demonerna i väster.

## Pirater