# Ikoner

## Nuvarande Ikoner

- Bankiren (Jasper Vargklot)
- De fem drakarna
- Trollkarlsrådet (Landet Heradi)
- Piratkungen (Castiel du Sin)
- Alvmodern (Skogsalver)
- Stjärnriddaren, (Sternekk, Korsriddaren)
- Druidkovenanten
- Skuggprinsen (Reinald Le Strat)
- Gastkungen (Dario Malic III)
- Kryddprinsen (Kerajaan + Istra)
- Stäppklanerna (Stäppalver och nomadfolk)
- Blodshövdingen (Ledaren över Itaus, krigare från norr)
- Matriarken, (Bestarnas moder)
- Cirkeln, (Nekromantiker)

## Tidigare Ikoner

- Diabolisten
- Orchfursten
- Stjärnprästinnan

## Bankiren

Jasper Vargklot

## De FEM Drakarna

, Eldmästarinnan, Den röda draken
, Blixtmästaren, Den blå draken
, Syramästrinnan, Den svarta draken
Tharos, Giftmästaren, Den gröna draken
Safira, Ismästarinnan, Den vita draken
Hon dödades av Gastkungen i den tolfte tidsåldern men återuppväcktes av Den blå draken när Mordecai av misstag öppnade en stormpåse och släppte lös en enorm storm mot Istra. Då kunde den blå draken ta del av den gigantiska blixtenergin och väcka sin syster.


## Trollkarlsrådet

Verksamma i Heradi.

## Piratkungen

Castiel du Sin

## Alvmodern

Skogsalver

## Stjärnriddaren

Sternekk (Korsriddaren)

## Druidkovenanten

## Skuggprinsen

Reinald Le Strat

## Gastkungen

Dario Malic III

## Kryddprinsen

Kerajaan + Istra

## Stäppklanerna

Stäppalver och nomadfolk

## Blodshövdingen

Ledaren över Itaus, krigare från norr

## Matriarken

Bestarnas moder

## Cirkeln

Nekromantiker
