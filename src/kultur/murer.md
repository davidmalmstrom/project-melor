# Murer

## Namn

För snart trehundra år sedan red krigaren Claude Mureur och hans vänner mot det som idag kallas för Aksis. Den tidigare staden Sarovar var boplatsen för Eldrot, draken som krossat huvudstaden av Derlis och dödat stora delar av befolkningen. Som från ovan hade han fått mod och ilska bröstet och bestämt sig för att samla sina vänner och dräpa draken.  Claude Mureur slogs i två dagar mot draken i de eldfyllda hålorna under den nedbrända staden. Sent på den andra natten såg några soldater från närliggande städer Claude Mureur kravla sig upp ur hål i marken. Med sig upp hade han beviset på att han dräpt draken, dess rykande hjärta. Claude Mureur var dock svårt brännskadad och dog några dagar senare. De överlevande i Derlis tog nu ett nytt namn för sig själva för att hylla den som räddat dem, de var nu Murer.

**Invånarnamn**: En Murese, flera Murer, Murisk konst
**Nuvarande land**: Västmuria, Sydmuria och Östmuria

## Överblick

Murerna är ett stolt folk som tror på förmågan att överkomma svårmod och segra mot ondska. De vördar sina förfäder och försöker att rita en linje från sig själv tillbaka till någon av de stora hjältarna.

## Fysiskt utseende

Bredaxlad, definierad haka, och rött långt hår är idealet bland murerna. De är få som har detta utseendet speciellt det röda håret. Hårfärgen är vanligast ljusbrunt till brunblont med svart hår som den största minoriteten. Det röda håret återfinns bland stäppalverna och är väldigt ovanligt. Det tyder på ett arv från någon utav Stäppklanerna.

## Geografi

De västra och sydliga delarna av Innanhavets kuster kallar murerna för sitt hem.

Vad finns i väst?
Vad finns i norr?
Stora floder?

## Hjältar och förfädersdyrkan

Tahar Yargai

## Tankar

Rött och guld

Ödet

Arvet från Adrar

Krigarkungar och  Trollkarlskungen

Hästar och relationer till Stäppfolken

Drakkejsardömet och enandet av Adriska havet folk. Förslavandet? av Cora.