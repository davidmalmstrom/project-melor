# Raser

## Människor

### Människor / Primärblod

Vanliga människor.

- **Normalt liv/Kortlivad**

- **Normal syn/Normal syn**

Lifespan 100 years

Tough skin

### Orcher / Krigsblod

Orcher, kallas kotoluk av alver och betyder smutsig krigare. De likt dvärgarna är experiment av alverna, orcherna var människor som förvreds av skogsalverna till stora krigare som skulle skydda alverna. De friades av den gröna draken Tharos.

#### Kortlivad/Kortlivad

För att inte kunna etablera sig gav skogsalverna orcher ett kort liv, avlade för krig behöver de inte kunna leva längre än de kan tjäna sina mästare på slagfältet. En orch lever runt 30 år, en riktigt gammal orch kan bli upp till 35 år.

#### Nattsyn/Normal syn

Skogsalverna gav orcherna deras förmåga att se i svagt ljus för att kunna anfalla tillsammans med dem under natten. Detta gör att så länge det finns någon form av ljuskälla kan en orch se som i dagsljus. Tas krigsblod som ett svagt blodsanlag får man normal syn.

#### Hårdhudad/Härdad

Orcherna har ovanligt seg hud som gör dem tåligare mot skada. Skogsalverna förvrängde människornas hud till att ta egenskaper av andra bestar med fjäll och hård hud. Det är därför det är möjligt att vissa orcher har ljusgrå fjäll på vissa delar av kroppen, speciellt runt nacken, ryggen och armarna. Orchernas hårda hud ger dem +2 RV/+1 RV.

#### Disciplinerad/Lojal

Orhcer har svårt att säga ifrån mot auktoriteter, tenderar att lyda dem som är mäktigare än dem själva. När dem lyder en mäktigare person är det svårare att manipulera dem. Detta gör dem till medgörliga underordnade och lojala livvakter. De har dock inget problem med att sätta sig upp mot sin ledare om hen visar sig svagare.

### Dvärgar / Gruvblod

Dvärgar, kallas itikalima av alver och betyder liten arbetare. Lifespan of about 300 years

#### Mörkersyn/Nattsyn

Dvärgar ser perfekt i mörker och behöver inte någon ljuskälla för att se.

#### Livskraftig/Långlivad

#### Culture

Dul is the title for leader and it's before the name: Dul Name Clan-name.

### Demonrörd / Calablod

## Alver

Elves are generally shorter than humans, having an average height of 160 cm (5 feet 3 inches) and weigh about 65 kg (65 pounds).

### Pure blood

(sus-sanas-ku rela kora-kotur)

Some elves are born with pure blood, their families have not mixed themselves with other elf people or other races for hundreds if not thousands of years.

This gives them the power of Blood memory, they can get insight from their ancestors through dreams and sometimes memories from days gone by can transfers to them.

### Culture

Family is not a concept in elven society, they have words for father, mother, son, daughter etc. but not for family. They do not see themselves as special because of the shared blood between them. They still love their parents and children but it's more common to not live together and do not feel a unity towards each other.

Lineage is an important concept for elves, since they do not live as families they take pride in their parents or siblings feats of power. Often attributing them as a "son of parent name" adding any titles, epithets or feats of power. Being able to recite ones lineage through either parents is seen as a great knowledge, you then know where you come from and what legacy it bears. They most often choose the parent with the most prestige as the

Surnames are any combination of toponymical, occupational, epithets and ancestral. Elves living in other cultures embrace their naming conventions but some may still cling to the elven societal naming.

The elven language is according by themselves the perfect language, there has not been any language reforms since the elves united under a common tongue. So any elf can read ancient elven writing and also speak it.

If an elf does not know the name of someone they use the word tanakoho for the person, meaning unnamed. They have no words for he, she or it, they use the name of the person, use the title of the person or try to describe the person instead, if that does not work they fall back on tanakoho.

"Took the bow who? Lovás the bow took."

"Took the bow who? The first ranger the bow took."

"Took the bow who? Tall, blond elf the bow took."

"Took the bow who? tanakoho the bow took."

A big weakness is that they see everything around them die. It fills most elves with sorrow that only grows over time. The withering of the surroundings, ageing of all other races and the breakage of love between people are some of the great sources of this sorrow. They have a word for sorrow that is literally dark-mind or night-mind which is often how they describe the feeling, a great veil of darkness that clouds their mind and sends them even further into sorrow. The pain is something that you can't prepare for and thus it is not spoken of often to young elves. They will have to deal with it when they reach a certain age, only then can they be taught how to deal with the agony.

"la lu-ku povidu ik tano" is a phrase, the literal meaning is "i your night-mind away take" and a translated one would be "take away your sorrow". It's the name of the ritual that some elves may request is performed on them. It's a ritual suicide that they choose instead of living with the pain and sorrow that is in their mind and heart. Usually only elves that are over a thousand year old start to think about. The performer of the ritual is often someone close to the elf such as a parent, sibling or partner, it can also be a close friend or someone that the elf looks up to. Kan liknas vid seppukku

### Mörkeralv / Nattblod

Called posana by themselves and it means night elf. Pale skin, dark hair

#### Eternal life/Lasting life

#### Dark vision/Low-light vision

#### Culture

The night elves don't have the tradition of oramali as the forest elves do, instead they use coins just as the majority of all people of Melor do.

Their society is ruled as a meritocracy, the most suitable elf for each role.

### Stäppalv / Vindblod

Called torasana by the elves, wind elf. Fair skin, blond or brown hair

#### Eternal life/Lasting life

### Skogsalv / Barkblod

Called rakasana by the elves and it means forest elf, bark elf or brown elf. Tan skin, brown or darker hair

#### Culture

Family is not a concept

Oramali or the tradition of giving a gift to someone or the exchange of gifts. It's most often used instead of monetary compensation. They have the terms give/receive coin for the actions of buy and sell items. oramali comes with four connotations as follows.

It shall have the appearance of being free and spontaneous, without stipulation of requiring a return gift.

The receiver is obligated to not only return a gift but to increase the value of the gift.

The two parties are now bound in social obligations to each other, to further the exchanges of gifts.

Failure to return a gift is considered a loss influence, respect, power and prestige.

The gifts can be small and the return also small but it should always increase in value, albeit by a small amount.

The time to return the gift should be brief but due to the eternal lifespan of elves their definition of brief can be hard to translate to other races.

As well as giving a gift oramali can also be used for giving an insult, which still follow the rules of oramali, except the first rule. The return insult can be a martial response in direct proportion to the insult.

#### Eternal life/Lasting life

#### Snabb/Lättfotad

Lättare att ta sig runt i omgivningen, enklare att klättra,hoppa. Smyger mycket snabbt, i normal hastighet.

## Halvling

Gnom är Civiliserad och Halvling är Nomad generellt sätt.

### Halvling / Nomadblod

### Gnom / Bosattblod???!?!?!

## Halvraser

De flesta raser kan få avkomma med de andra raserna och producera en halvras mellan de då. Detta har inte alltid varit fallet. Det var i slutet av den trettonde tidsåldern som det uppdagades att halvraser kunde existera då en halvorch hittades i den Vilda skogen i närheten av Eld. Halvraserna har en sak gemensamt är att de bara kan få avkomma med en varelse av samma halvras.

- Ta bort rasernas attributmoddar

- Raser har olika HP-mod, de flesta "medium"-sized har lika

- De får vara obalanserade

- Mer fokus på intressanta karaktärer istället för power

### Halvalver

### Halvorcher

## Cala

Demoner och djävlar är av samma ras men tror på olika sätt att leva. Demoner vill vara helt fria och låta känslorna flöda medan djävlar förvisso är öppna till känslor men tror på en värld där lagar och regler styr.

### Demon

### Djävul

## Blood traits

### Ageing

#### Short life

Maximum of 50 years

#### Normal life

Maximum of 100 years

#### Long life

Maximum of 300 years

#### Lasting life

Maximum of 500 years

#### Eternal life

Will never die

### Vision

#### Normal

#### Low-light vision

#### Dark vision

### Skin

#### Normal

#### Tough

#### Hardened

### Loyalty

#### Loyal

#### Disciplined

### Speed

#### Normal

#### Lightfooted

#### Fast
