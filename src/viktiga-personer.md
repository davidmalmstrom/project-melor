# Historiska personer och fraktioner

Viktiga personer inom varje ras.

## Sýradar Toratáh

Mörkeralvprins från Esamila

## Merelle

En av Gastkungens främsta neoromantiker och Lenores adoptivmamma. Ursprungligen från Horizon där hon studerade magi men blev bannlyst för hennes försök att återuppliva de döda. Något som varit närmast en tvångstanke sen hennes dotter gått bort. Merelle föll in i att studera nekromanti då att få liv i dem döda var ett naturligt steg på vägen att helt återuppliva dem.

## Rosklo

## Teirina

## Tallon och Netulah Ödesvävare

Tallon, Ledare för Nemizenkulten.

## Ärkemagikern och Glabrezou

Rosa kristallerna
Förstörandet av Ärkemagikerns stad

## Rödhandsordern

## Magikern Cole

## Constantine Deus

Constantin Deus, found him on a website. Not sure whom made him. Good description of what he looked like in his armor, before he died.

## Solsken Lilastoft

Prinsessan som Tobbes krigare skulle finna.

## aluna

Spelare: Micke
Kommer från Svartalvstaden Maalenor
Riktigt namn: Vierintra: Dark Envoy, Araberus: Daughter of the Depths
Täcknamn: Alaunraema: Lightning Crafter, Myneld: Honoured of the Arcane
OUT: Jag är en arvinge till mitt adelshus och min mor ligger för döden.
Backgrounds

- Drow noble +4
- Enoy of the houses +2
- Magic acolyte +2

Icons

- Elf Queen, Postive 2
- Diabloist, Negative 1

True Magic Items

- Cruel Scepter
- Amulet of Property

## Castiel Du sin

### Rykten

Castiel Du Sin är ett namn som har sitt ursprung i den Tolvte tidsåldern då bars det av en ökänd orchpiratkapten som satt skräck i Melor. Namnet ska enligt sägen betyda "Den som vandrar på blod" I den trettonde tidsåldern bär en halvlängdsman samma namn, många påstår att det inte är mycket mer än en entusiast som tagit namnet som en hyllning till en forna pirat då denna halvlängdsman också är pirat. De mer skrockfulla piraterna viskar om reinkarnation imagistormen, för är det inte den mytomspunna fanan som Castiel Du Sin den forna pirat kungen hade på sitt skepp? Är inte Natthäxan det mytomspunna skeppet som Castiel Du Sin seglade i den tolfte tidsåldern? Det finns få kanske ingen levande pirat från den tiden som med sanning kan bekräfta dessa rykten. De flesta som själv mött denna halvlängdsman vet att han med en obestridd självklarhet hävdar han är just den han utger sig för att vara. Det finns vissa rykten bland det fattigare folket att Castiel Du Sin ger guld till dem utsatta, dom som glömts bort och blivit förtappade.

### Utseende

Bard och Rogue som har en pirats utseende. Castiels ögon är lila, dess intensiva färg förhöjs av svart smink som omger dem. Hans öron är piercade med diverse ringar av silver och guld. Det mörka håret sträcker sig ned för hans axlar, håret är tillbakadraget och ordnat i flätor och dreadlocks som pryds av pärlor, ädelstenar samt ringar av ädelmetaller. En lilasvart sjal är virad runt hans huvud, där han bär en sliten och solblekt kaptenshat. Ansiktet är distinkt och spetsigt någonting som förhöjs av hans mörka pipskägg som är sammansatt i en fläta tillsammans med ett fåtal pärlor. Hans klädnad är extravagant, han skulle lika gärna kunna vara någon adlig med lite väl vågat mode såväl som en piratkapten. Rocken är kungsblå med silverbroderier under den bär han en kråsskjorta. Byxorna han bär är vida och pösiga, brandgula med röda exotiska mönster. Byxorna är åtsnörda strax under knäna men går ändock ner halvvägs längs smalbenen på grund av den stora mängd tyg som använts till dem. Strax under knäna börjar ett par vackra läderstövlar med ingraveringar. Dock är det mest utmärkande draget med dessa skor den närmast ridikulösa mängden spännen. Vid hans bälte hänger ständigt en huggare samt pistol tillsammans med några pungar och en hållare för vattenskinn. I hans hand är allt som oftast en pipa eller vinflaska. En lätt doft av alviskt vin efterföljer allt som oftast Castiel.
Castiel verkar ständigt vara i rörelse även då han tillsynnes är helt oupptagen med något ärende. Rörelserna är ofta små, korta, gungande och extremt graciösa. Ibland då det blir längre, yvigare rörelser kan det se ut som en orms slingrande. Han tycks inte särskilt muskulös, däremot är han extremt senig och tycks vara ganska härdad.

### Historia

**Tidiga liv**
Sedan han återvände från stormen har Castiel Du Sin växt upp bland resande halvlängdsmän. Piratådran hade han redan genom sitt tidigare liv. Men det tog tid innan han insåg vem han var. Underhållaren och tjuven är passande ord för att beskriva hans tidiga år. Tillslut av en rad anledningar började han arbeta för Höginkvisitorn som spion. På grund av sina kunskaper som bard gjorde han sig ypperlig för jobbet. Däremot var han tvungen att slipa på sin etikett för att kunna föra sig bland de högre skikten. Bland de lägre kasten var det ingen tvekan om att Castiel visste hur man navigerade. Efter några år i inkvisitionens tjänst började allt fler av minnen bli tydliga. Han hade i sin tid bland de resande halvlängdsmännen endast fått korta minnesbilder, drömmar och andra kryptiska tecken på sitt tidigare liv. Men nu började det tidigare livets minnen flyta samman med hans nuvarande. Detta kulminerade och Castiel lämnade inkvisitionen bakom sig fullt övertygad om att hans syfte var att finna på havet. Han tog värvning vid en piratbesättning, till sin chock så verkade han vara en naturbegåvning. Kaptenen på skeppet noterade snabbt den unga halvlängdsmannens fallenhet vilket gjorde att han snabbt fick ta över vissa ansvar på skeppet. Efter några år så beslutade Castiel sig dock för att ta en del av det guld han tjänat ihop och köpa sig loss ifrån besättningen för att ge sig ut på äventyr och söka skapa sin egen besättning.

**Senare liv**
Castiel du Sin lyckas få tag på alla stenarna och "vinner" på så sätt de rosa kristallernas kamp. Dock är han inte säker på vad han ska göra med dessa stenar, som igentligen var ett vapen. Det har snarare handlat om att inte låta någon annan få tag på dem. Sedan bestämmer sig Castiel du Sin för att använda dem och tar trots sina kompanjoners rådan vapnet mot Orc fursten och Dvärg fursten. Han dräper Orc fursten med stenen och drar sedan honom tillbaka med hjälp av en pakt han gjort med döden. Vilket tvingar Orc fursten att göra som han säger, eller dö. Sedan slåss dom tillsammans mot Dvärg fursten. Det slutar i att båda ikonerna faller. Castiel du Sin tar sin plats som Pirat Kungen och den 14'e tidsåldern skapas. Nu i formen av en orch då han återföddes efter att han använt vapnet, som i praktiken dödade honom då han var vapnet.

**Icon**
Castiel du sin tar sin plats som pirat kung och deklarerar att pirat koden åter igen skall gälla för alla som står under den svarta flaggan. Castiel inleder en våg av attacker mot kuststäder. Många plundras och efter att dessa nyheter spridit sig så väljer många att betala tribut för att slippa se sina städer brännas till marken.
Flera fristäder skapas och Castiel placerar ut folk han litar på att agera som guvenörer på dessa.
Castiel eftersöker sitt gamla skepp.
Prinsessan Kyra Stjärnlund

## Emma ”Hermelinen” Lavinder

Hermelinen hade en något stor del i kriget om de rosa kristallerna. Hon var inte allierad med Lenore, Castiel du Sin och deras följe men hjälpte dem vid ett flertal ställen. Hon är en mycket kapabel tjuv och drar sig för att döda intelligenta varelser det oftast inte är värt det och hon är ingen lönnmördare. Hon har bra kontakter och fixade fram ritningar över Drakkejsarens valv åt Castiel du Sin.
Hon växte upp i nordstaterna som då var en del av Drakkejsardömet hos Chandler Vargklot och hans son Jasper.
Det var magikern Cole som räddade henne från nekromantikern Kiströta i östra Spindelskogen på Tavros.
Hon var i nordstaterna när Jasper funnit draken Mekarth och Castiel du Sin var ute efter hans magiska bok.

## Jasper Vargklot

Son till Chandler Vargklot en adelsman från De fria nordstaterna. Jasper hade en liten påverkan under kriget om de rosa kristallerna. Jasper växte upp i närheten av den lilla träfällningsbyn Timmerklocka och levde ett gott liv till sin moders död. Hans mor hade blivit en varulv när Jasper var 14 år, hon blev sedan dräpt av magiker under tredje månljuskriget. Ett krig är att ta i, det är ett återkommande problem i nordstaterna med varbestar. Jasper blev både förkrossad och rasande och märkte då att magi flöda ur honom, han smet hemifrån för att ta reda på mer om magi.
När Jasper till slut kom tillbaka efter några år, ville hans far Chandler inte ha med honom att göra. Han blev bara påmind om de som dräpt hans fru, Jasper försökte förgäves visa att med sina krafter skulle han dels förgöra alla magiker och alla varulvar. På det sättet skulle han få hämnd på sin mor.
Jasper flydde ut i skogen, sprang för allt han orkade och hittade till sist en plats att övernatta på, en varm grotta. Grottan var varm för en röd drake som hette Mekarth bodde längre in i grottan och andades varm luft som spred sig genom gångarna.
Morgonen därpå bestämde sig Jasper för att utforska grottsystemet för att se var den varma luften kom från. Han fann då att det fanns fällor i gångarna när han precis höll på att trampa på en knappt synbar tråd. Han blev mer vaksam, drog sin dolk och gick långsamt längre in. Trevandes i mörkret kunde inte ens hans magi lysa upp tunneln, han tog ett försiktigt steg fram, fastnade med foten i en snara och slungades upp och ned.
Ekandes inom berget hörde han elaka skratt som kom närmare. Han såg först ett par lysande ögon som sedan blev två, fyra, slutligen åtta par lysande ögon. Jasper han knappt hålla sig för näsan när en stickande doft slog emot honom innan ett hårt slag fick honom att tappa medvetandet.
Jasper vaknade till en frän doft av svavel och en enorm hetta. Han försökte vrida sig, men han var bunden runt händer och fötter. Framför sig, bara några meter bort såg han ett stort rött huvud prytt av röda fjäll och ett par gula stirrande ögon. Jasper satt blixtstilla och stirrade tillbaka, helt frusen inför den eldsprutande besten. Mekarth tog ton och berättade för honom om hur magikerna jagat bort honom från skogen och städerna. Om hur manulvarna dräpt av mycket av de ätbara djuren i skogen. Jasper som nu lugnat ned sig yttrade sig om att en drake minsann inte borde ha sådana enkla problem. Draken blängde surt på Jasper, röt ifrån med att hans enkla problem endast var ett problem på grund av magikerna. ”Då ska jag dräpa dem alla”, brast Jasper ut. Förvånat tittade Mekarth på den lilla människan, skrattade hånfullt och sade: ”Ja men vad bra.”.
Varelserna med lysande ögon tog detta som en order och gick fram och lossade hans rep. Jasper mindes att dessa varelser kallas för kobolder och är ett drakliknande släkte som dyrkar drakar.
Han gnuggade sina handleder som värkte efter repen och tog hastigt tillbaka sin dolk som kobolderna stulit. Han bugade inför den hånleende draken och började gå ut ur grottsalen. ”Vad heter du pojk? Tänkte att det kunde vara kul att veta vem som dog för min skull.”, frågade draken. Han vände lite lätt på huvudet och sade med stor självsäkerhet ”Jasper Vargklot. Jag kommer tillbaka snart.”
En dag senare återvände Jasper blodig och mörbultad tillbaka till grottan. ”DRAKE”, skrek han. ”Var är du? De är. Alla. Döda!” Han gick förbi de dolda fällorna, blängde trött och elakt mot de illaluktande kobolderna och kom in i den röda drakens stora sal. ”De är alla döda”, sade han innan han stupade av utmattning på det varma grottgolvet.
Några dagar senare (Jasper vet inte då han var medvetslös, drakar lever för länge för att bry sig och kobolderna kunde inte direkt räkna, så några dagar fick vara bäst) vaknade Jasper upp. Nu på en bädd av granris i ena änden av drakens sal.
”Sovit gott min vän?”, frågade draken.
”Min vän?”, frågade Jasper tillbaka.
”Ja, är vi inte det då? Jag gav dig en bädd att sova på och du dräpte dem som långsamt höll på att döda mig.”
”Du vet mitt namn, men inte jag ditt.”
”Mekarth”
”Va? Ingen titel, inget «dräpare av män» eller inte ens «den röde»?”
”Jag har inte gjort något stort för att förtjäna något sådant. Sedan vet jag inte om jag vill ha en sådan titel heller. Det jag dock skall göra är att hjälpa dig, då jag är dig skyldig en stor tjänst för räddandet av mitt liv.”
Jasper och Mekarth började den dagen att plundra skatter, dräpa manulvar och blev starkare både inom magi jämte självsäkerhet. Tills en dag då Jasper dräpt ett gäng med orcher som flytt in i skogen. Då han i deras packning fann en bok med mithrilomslag. På den första sidan av boken stod det «Världens Eviga Evighet: Den kompletta samlingen i 16 volymer» och en spökliknande röst frågade Jasper vad han ville veta. Han stängde snabbt boken och sprang tillbaka till Mekarth.
Han visade upp den underliga boken som återigen frågade vad han ville veta. Mekarth frågade: ”Vad är du?”. ”En bok”, svarade boken.
”Jag tror vi måste vara lite mer speciella med frågorna.”, påpekade Jasper till Mekarth.
”Vad är alla typer av frågor som du kan svaret på?”, frågade Jasper.
”Alla.”, svarade boken.
”Vem äger boken just nu?”, frågade Jasper.
”Du.”
”Hur många citronkakor fick jag av min mor efter att jag hade brutit benet när jag var 9 år?”
”Sjutton.”
”Vad var det första jag sade till Mekarth?”, frågade Jasper och flinade mot draken.
Mekarth suckade lätt.
”En drake borde minsann inte ha sådana enkla problem som magiker och manulvar.”
”Jag tror att boken talar sanning. Är någon ute efter dig just nu?”
”Ja.”
”Vem är ute efter dig just nu?”
”Castiel du Sin, Emansuel, Alusaté, Arók, Kain, Lynadis, Eliseo Celtalor, Searle Bariar, Kane Werika, Kareem Cabrera, Kade Vaux, Nedr Vitthar, Roselin Simonet, Emma Lavinder…”
”Sluta läs upp namn. Vem som är ute efter dig är närmast just nu?”
”Castiel du Sin.”
”Mekarth, vi måste fly.”
Jasper klättrade upp på drakens rygg och de flög iväg från grottans värme ut i den kyliga eftermiddagen. År senare, efter att boken bytt händer ett par gånger bestämde sig Jasper för att investera sina pengar som han och Mekarth samlat ihop med hjälp av bokens hjälp. Paranoid nog så lämnade han aldrig boken utan håller sig mest inomhus i ett stort slott på en ö utanför Tavros kust. Vaktad av mängder av vakter, fällor och en drake. Inlåst tillsammans med det som flest i världen vill åt, endast med sin vän Merkarth sitter han och tar reda på världens hemligheter och egenheter. Utan att aldrig våga upptäcka dem själv. Vargklotsbanken kan vara det läskigaste som skapats efter Världens Eviga Evighet: Den kompletta samlingen i 16 volymer. En bank som lånar ut pengar, och som du aldrig kan gömma dig från. Som kan ta reda på allt om dig, som inte är rädda att ta till omoraliska metoder för att få tillbaka sina pengar och som har en mäktig häxmästare & drake som ägare. På grund av sitt rykte av att alltid få tillbaka sina pengar och hur grymma dem kan vara, har många andra banker höjt räntorna, för att de kan. Antingen lånar man av de med högre räntor, eller av banken som styrs av en allvetande häxmästare.
