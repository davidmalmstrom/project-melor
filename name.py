#coding: utf8
import os
import json
import random

NUM_NAMES = 10

with open("./resources/names.json", encoding="utf8") as names_file:
    name_data = json.load(names_file)
    name_male = name_data["muria"]["male"]
    name_female = name_data["muria"]["female"]
    name_lastnames = name_data["muria"]["lastnames"]

    males = random.sample(name_male, NUM_NAMES)
    lastnames = random.sample(name_lastnames, NUM_NAMES)

    print("Male")
    for n in range(NUM_NAMES):
        print(males[n] + " " + lastnames[n])
    
    print("")

    females = random.sample(name_female, NUM_NAMES)
    lastnames = random.sample(name_lastnames, NUM_NAMES)

    print("Female")
    for n in range(NUM_NAMES):
        print(females[n] + " " + lastnames[n])
